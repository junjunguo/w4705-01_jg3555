#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 16:18:45 2017

@author: Jun

Classify.py takes train and test files as command line arguments; trains and 
saves best-scoring model to a file, test the model on a test set and prints accuracy.

Input: Train.txt and test.txt
Output: Model.pkl, Accuracy Printed and Vectorized Test Data (Feature Names)
"""

import sys, os
import numpy as np
import re 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.pipeline import Pipeline
from time import time
import pickle
from optparse import OptionParser

"""
Output of the function is two seperate matrices:
    the first matrix is the text contents of all the tweets with web links and 
    party information removed;
    the second matrix is the party matrix with 1 stands for Democrat and 0 stands 
    for Republican.
"""
def import_document(filepath):
    with open(filepath) as text:
        tweets,results = process_text(text)
        return tweets,results


def process_text(csv_reader):    
    tweets_matrix = []
    result_matrix = []
    
    #References: http://xpo6.com/list-of-english-stop-words/

    stop_words = ["a", "about", "above", "about", "across", "after", "afterwards",\
                  "again", "against", "all", "almost", "alone", "along", "already",\
                  "also","although","always","am","among", "amongst", "amoungst", \
                  "amount",  "an", "and", "another", "any","anyhow","anyone",\
                  "anything","anyway", "anywhere", "are", "around", "as",  "at",\
                  "back","be","became", "because","become","becomes", "becoming",\
                  "been", "before", "beforehand", "behind", "being", "below",\
                  "beside", "besides", "between", "beyond", "bill", \
                  "both", "bottom","but", "by", "call", "can", "cannot", "can't",\
                  "co", "con", "could", "couldn't", "cry", "de", "describe", \
                  "detail", "do", "done", "down", "due", "during", "each", "eg",\
                  "eight", "eighty", "eighteen", "either", "eleven","else",\
                  "elsewhere", "empty", "enough","follow","unfo","follower"\
                  "etc", "even", "ever", "every", "everyone", "everything", \
                  "everywhere", "except", "few", "fifteen", "fify", "fill", \
                  "find", "first", "five", "for", "former", "formerly", "forty",\
                  "found", "four", "from", "front", "full", "further", "get", \
                  "give", "go", "had", "has", "hasnt", "have", "he", "hence",\
                  "her", "here", "hereafter", "hereby", "herein", "hereupon",\
                  "hers", "herself", "him", "himself", "his", "how", "however",\
                  "hundred", "ie", "if", "in", "inc", "indeed", "interest",\
                  "into", "is", "it", "its", "itself", "keep", "last", "latter",\
                  "latterly", "least", "less", "ltd", "made", "many", "may",\
                  "me", "meanwhile", "might", "mill", "mine", "more", "moreover",\
                  "most", "mostly", "move", "much", "must", "my", "myself", "name",\
                  "namely", "neither", "never", "nevertheless", "next", "nine",\
                  "no", "nobody", "none", "noone", "nor", "not", "nothing", "now",\
                  "nowhere", "of", "off", "often", "on", "once", "one", "only",\
                  "onto", "or", "other", "others", "otherwise", "our", "ours",\
                  "ourselves", "out", "over", "own","part", "per", "perhaps", \
                  "please", "put", "rather", "re", "rt", "same", "see", "seem",\
                  "seemed", "seeming", "seems", "serious", "several", "seven",\
                  "seventy", "seventeen","she", "should", "show", "side", "amp"\
                  "since", "sincere", "six", "sixteen","&amp", "omg", "lol", "is"\
                  "sixty", "so", "some", "somehow", "someone", "something", "sometime",\
                  "sometimes", "somewhere", "still", "such", "system", "take",\
                  "ten", "than", "that", "the", "their", "them", "themselves", \
                  "then", "thence", "there", "thereafter", "thereby", "therefore", \
                  "therein", "thereupon", "these", "they", "thickv", "thing", "third",\
                  "this", "those", "though", "three", "through", "throughout", \
                  "thru", "thus", "to", "together", "too", "top", "toward",\
                  "towards", "twelve", "twenty", "two", "un", "under", "until", \
                  "up", "upon", "us", "very", "via", "was", "we", "well", "were",\
                  "what", "whatever", "when", "whence", "whenever", "where", \
                  "whereafter", "whereas", "whereby", "wherein", "whereupon",\
                  "wherever", "whether", "which", "while", "whither", "who",\
                  "whoever", "whole", "whom", "whose", "why", "will", "with", \
                  "within", "without", "would", "yet", "you", "your", "yours", \
                  "yourself", "yourselves", "the", "i","ah{1,}","oh{1,}","uh{1,}",
                  "a{1, }","o{1, }","so{1,}","\'", "followers", "unfollow",\
                  "unfollowers", "unfollower"]
    
    stop_word_string = '\W(' + '|'.join(stop_words) + ')\W'
    
    result = 0
    for row in csv_reader:
        cleanrow_no_url = re.sub('http.+\t','\t',row.lower())

        english_words = re.findall('\w+', cleanrow_no_url)        
        party = english_words[-1]
        content = "  ".join(english_words[:-1])

        cleanrow_no_stopword = re.sub(stop_word_string, ' ', "  "+content+"  ")        
        cleanrow_no_isolatednum = re.sub('\W(\d+|\d+\w+)\W', ' ', cleanrow_no_stopword)
        cleanrow_stemmed = stemming(cleanrow_no_isolatednum)
        tweet_content = cleanrow_stemmed

        if "democrat" in party:
            result = 1
        elif "republic" in party:
            result = 0
                
        tweets_matrix.append(tweet_content)
        result_matrix.append(result)
    
    return tweets_matrix, result_matrix


def stemming(string):
    vowels_pair_lookback = '(?<=(oo|ee|ii|aa|uu|oi|io|eo|oe|oa|ao|ou|uo|ae|ea|ia|ai|ei|ie|eu|ue|iu|ui|au|ua))'
    cleanrow_no_plural = re.sub('(?<!\s)(sses|ssed|ssing)\s','ss ',string)
    cleanrow_no_plural = re.sub('(?<!\s)(shes|shed|shing)\s','sh ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<!\s)(ches|ched|ching)\s','ch ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<!\s)(ghes|ghed|ghing)\s','gh ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(ses|sed|sing)\s','se ',cleanrow_no_plural) #exception biased, biases
    cleanrow_no_plural = re.sub('(xes|xed|xing)\s','x ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(ies|ied)\s','y ',cleanrow_no_plural)  #can cause problem for words such as series (sery), movies(movy), lies(ly), ties
    cleanrow_no_plural = re.sub('s\s',' ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('men\s','man ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('oes\s','o ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(cing|ced)\s','ce ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(ving|ved)\s','ve ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<!\s)(hing|hed)\s','h ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<!\s)(wing|wed)\s','w ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<!\s)(ying|yed)\s','y ',cleanrow_no_plural) #No exception one --
    cleanrow_no_plural = re.sub('(zzing|zzed)\s', 'zz ', re.sub('(?<!(z|\s))(zing|zed)\s','ze ',cleanrow_no_plural))
    cleanrow_no_plural = re.sub('(gging|gged)\s','g ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(ging|ged)\s','ge ',cleanrow_no_plural)
    
    cleanrow_no_plural = re.sub('(tting|tted)\s','t ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u))(ting|ted)\s','te ',\
                         re.sub(vowels_pair_lookback+'(ting|ted)\s', 't ',\
                         re.sub('(?<!(o|e|i|a|u))(ting|ted)\s','t ',cleanrow_no_plural)))

    cleanrow_no_plural = re.sub(' ad ',' add ',re.sub('(dding|dded)\s','d ',cleanrow_no_plural))
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u))(ding|ded)\s','de ',\
                         re.sub(vowels_pair_lookback+'(ding|ded)\s', 'd ',\
                         re.sub('(?<!(o|e|i|a|u))(ding|ded)\s','d ',cleanrow_no_plural)))
        
    cleanrow_no_plural = re.sub('(?<=(o|i|a|u|r))(ring|red)\s','re ',\
                         re.sub(vowels_pair_lookback+'(ring|red)\s', 'r ',\
                         re.sub('(?<!(o|i|a|u|r|\s))(ring|red)\s','r ',cleanrow_no_plural)))
        
    cleanrow_no_plural = re.sub('(?<=(o|i|a|u|k|e))(king|ked)\s','ke ',\
                         re.sub(vowels_pair_lookback+'(king|ked)\s', 'k ',\
                         re.sub('(?<!(o|i|a|u|e|k|\s))(king|ked)\s','k ',cleanrow_no_plural)))
        
    cleanrow_no_plural = re.sub('(?<=(o|i|a|u|e|m))(ming|med)\s','me ',\
                         re.sub(vowels_pair_lookback+'(ming|med)\s', 'm ',\
                         re.sub('(?<!(o|i|a|u|e|m|\s))(ming|med)\s','m ',cleanrow_no_plural))) #Wyoming is now wyome
        
    cleanrow_no_plural = re.sub('(nning|nned)\s','n ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<=(o|i|u))(ning|ned)\s','ne ',\
                         re.sub(vowels_pair_lookback+'(ning|ned)\s', 'n ',\
                         re.sub('(?<!(o|i|u|\s))(ning|ned)\s','n ',cleanrow_no_plural)))
    
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u|g|b|k|f|c|d|p|t))(ling|led)\s','le ',\
                         re.sub(vowels_pair_lookback+'(ling|led)\s', 'l ',\
                         re.sub('(?<!(o|e|i|a|u|g|b|k|f|c|d|p|t|\s))(ling|led)\s','l ',cleanrow_no_plural)))
        
    cleanrow_no_plural = re.sub('(pping|pped)\s','p ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u))(ping|ped)\s','pe ',\
                         re.sub(vowels_pair_lookback+'(ping|ped)\s', 'p ',\
                         re.sub('(?<!(o|e|i|a|u|\s))(ping|ped)\s','p ',cleanrow_no_plural)))
        
    cleanrow_no_plural = re.sub('(bbing|bbed)\s','b ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u))(bing|bed)\s','be ',\
                         re.sub(vowels_pair_lookback+'(bing|bed)\s', 'b ',\
                         re.sub('(?<!(o|e|i|a|u|\s))(bing|bed)\s','b ',cleanrow_no_plural)))
    
    cleanrow_no_plural = re.sub('(ffing|ffed)\s','f ',cleanrow_no_plural)
    cleanrow_no_plural = re.sub('(?<=(o|e|i|a|u|\s))(fing|fed)\s','fe ',\
                         re.sub(vowels_pair_lookback+'(fing|fed)\s', 'f ',\
                         re.sub('(?<!(o|e|i|a|u|\s))(fing|fed)\s','f ',cleanrow_no_plural)))
    
    return cleanrow_no_plural



def Vectorizer(train_matrix, test_matrix, lower,upper):
    token_pattern = '[A-Za-z0-9]{2,}'
    ngram_range = (lower, upper)
    
    vectorizer = CountVectorizer(token_pattern = token_pattern,\
                                     ngram_range = ngram_range)
        
    vectorized_train = vectorizer.fit_transform(train_matrix)
    vectorized_test  = vectorizer.transform(test_matrix)
    features_names = np.asarray(vectorizer.get_feature_names())

    return vectorized_train, vectorized_test, features_names


def feature_selection(train_set, test_set, train_y, k = 50):
    chi = SelectKBest(chi2, k)
    gram_train_new = chi.fit_transform(train_set, train_y)
    gram_test_new = chi.transform(test_set)
    
    return chi, gram_train_new, gram_test_new
    
"""
Benchmark helped me search through different methods
From SKlearn
"""
def benchmark(clf, train_set, test_set, train_y, test_y):
    print(">>>"*10)
    print('Training')
    print(">>>"*10)
    print(clf)
    
    t0 = time()
    clf.fit(train_set.todense(), train_y)
    train_time = time() - t0
    print('Testing')
    print('>>>'*10)
    
    t0 = time()
    predict = clf.predict(test_set.todense())
    test_time = time() - t0
    
    score = metrics.accuracy_score(test_y, predict)
    #clf_descr = str(clf).split('(')[0]
    
    print(metrics.classification_report(test_y, predict))
    
    return clf, score, train_time, test_time

def Grams_Training(tweets_train, result_train, tweets_test, result_test):
    ls = LinearSVC(max_iter = 5000)
    lg = LogisticRegression(max_iter = 5000)
    gnb = GaussianNB()
    mnb = MultinomialNB()
    bnb = BernoulliNB()
    
    alpha = [0.1, 0.25, 0.5, 0.75, 1.00]
    c = [1,10,100]
    penalty = ('l1', 'l2')
    
    lsg = GridSearchCV(ls, {'C': c})
    lgg = GridSearchCV(lg, {'penalty': penalty, 'C': c})
    mbg = GridSearchCV(mnb, {'alpha': alpha})
    bbg = GridSearchCV(bnb, {'alpha': alpha})
    
    k = [10000, 5000, 5000]
    for i in range(1,4):
        gram_train, gram_dev, feature_names = Vectorizer(tweets_train, tweets_test, i, i)
        print(str(i)+" - Gram vectorized")
        
        chi, gram_train_new, gram_test_new = \
            feature_selection(gram_train, gram_dev, result_train, k[i-1])
        print("Feature Selected")
        temp = 0
        
        for method in (lsg, lgg, mbg, bbg, gnb):
            clf, score, train_time, test_time =\
            benchmark(method, gram_train_new, gram_test_new, result_train, result_test)
            
            if score > temp:
                temp = score
                pickle.dump(clf, open('mod'+str(i)+'.pkl', 'wb'))
        
        selected_feature = feature_names[chi.get_support(indices=True)]
        pickle.dump((gram_test_new,selected_feature, result_test), open('dev'+str(i), 'wb'))
    

    
#    
if __name__ == '__main__':
    print('Loading Documents:... ')
    tweets_train, result_train = import_document(sys.argv[1])
    tweets_test, result_test = import_document(sys.argv[2])
    
#    tweets_train, result_train = import_document('train.txt')    
#    tweets_test, result_test = import_document('dev.txt')    
    print('Documents Loaded. Vecotorizing Text Document...')
#    Grams_Training(tweets_train, result_train, tweets_test, result_test)

    gram_train, gram_test, feature_names = \
                     Vectorizer(tweets_train, tweets_test, 1, 1)
    print('Text Vectorized...')
    
    print('Feature Selection...')
    chi, gram_train_new, gram_test_new = \
    feature_selection(gram_train, gram_test, result_train, 44400)

    
    print('Training Model...')
    
    clf = MultinomialNB()
    
    print(chi)
    print(clf)
    clf.fit(gram_train_new.todense(), result_train)
    
    print('Testing Model...')
    y_hat = clf.predict(gram_test_new.todense())
    print('Accuracy is: ')
    print(metrics.accuracy_score(result_test, y_hat))
    print(metrics.classification_report(result_test, y_hat))
    
    """
    Saving the trained model as model.pkl
    """
    pickle.dump(clf, open('model.pkl', 'wb'))
    
    print("Best-performing model saved")
    """
    Saving the vectorized testfile with its result and feature names
    """
    
    selected_feature = feature_names[chi.get_support(indices=True)]
    pickle.dump((gram_test_new,selected_feature, result_test), open('test', 'wb'))
    
