#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 20:55:18 2017

@author: Jun

Analyze.py takes a trained model and the vectorized test data as command line
arguments and prints the top 20 features and a contingency table to the console. 

Input: Model Pickle from Classify.py and Vectorized Test from Classify
Output: Top 20 features and A Contingency table
"""

import numpy as np
import pickle, sys
from sklearn.metrics import confusion_matrix
from sklearn.feature_selection import SelectKBest, chi2
import pandas as pd

def contingency_table(model, vectorized_test, test_results):
    print("The Contingency Table is: ")
    y_predict = model.predict(vectorized_test.todense())
    contingency = pd.DataFrame(confusion_matrix(test_results, y_predict),\
                 index=['Actual_Republicans', 'Actual_Democrats'],\
                 columns= ['Predicted_Republicans', 'Predicted_Democrats'])
    print(contingency)

def top20_features(model, vectorized_test, feature_names, test_results):
    ch = SelectKBest(chi2, k =20)
    temp = ch.fit_transform(vectorized_test, test_results)
    temp = ch.get_support(indices= True)
    
    fe20 = feature_names[temp]
    
    if 'Multinomial' in str(model).split('(')[0]:
        de20 = model.feature_log_prob_[:,temp][1]
        re20 = model.feature_log_prob_[:,temp][0]    
        order  = np.argsort(-np.abs(de20-re20))
        data = np.stack((de20,re20),axis = 1)
        top20 = pd.DataFrame(data[order],\
                         index = fe20[order],\
                         columns = ['democrat', 'repbulican'])
    
        print("Top 20 Features and their conditional probabilities")
        print(top20)
    else:
        print(fe20)
        
    
if __name__ == '__main__':
    print('Loading Documents:... ')
    model = pickle.load(open(sys.argv[1], 'rb'))
    vectorized_test, feature_names, test_results = pickle.load(open(sys.argv[2], 'rb'))
    
#    model = pickle.load(open('model.pkl', 'rb'))
#    vectorized_test, feature_names, test_results = pickle.load(open('test', 'rb'))
    
    contingency_table(model, vectorized_test, test_results)
    top20_features(model, vectorized_test, feature_names, test_results)
    
  