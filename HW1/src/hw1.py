#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 02:16:07 2017

@author: Jun Guo (jg3555)

Natural Language Processing: HW1 Wapper
"""

from subprocess import call
import sys

if __name__ == "__main__":
    
    call(['python3','classify.py', sys.argv[1], sys.argv[2]])
    call(['python3','analyze.py','model.pkl', 'test'])
#    call(['python3','analyze.py', 'mod1.pkl', 'dev1'])
#    call(['python3','analyze.py', 'mod2.pkl', 'dev2'])
#    call(['python3','analyze.py', 'mod3.pkl', 'dev3'])

