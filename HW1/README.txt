Jun Guo jg3555@columbia.edu HW1

Use the following line: 
python3 hw1.py [path to training file] [path to testing file]

Can also use this to obtain best model:
python3 classify.py  [path to training file] [path to testing file]

To read the result and top 20 features: 
python3 analyze.py model.pkl test


Features include tweets content, hashtags, username etc. 
Limits include imperfect stemming process, unable to classify tweets unrelated to politics, unable to classify neutral tweets. 