from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
import sklearn.metrics
import sklearn.neighbors

import matplotlib.pyplot as plt
import numpy as np


print("Loading 20 newsgroups dataset for categories:")
data_train = fetch_20newsgroups(subset='train', shuffle=True, random_state=42)
data_test = fetch_20newsgroups(subset='test', shuffle=True, random_state=42)
print('data loaded')

'''Create tf-idf vectors for the input'''
vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.9,
                                 stop_words='english')
X_train = vectorizer.fit_transform(data_train.data)
X_test = vectorizer.transform(data_test.data)
y_train = data_train.target
y_test = data_test.target

'''Train a K-Neighbors Classifier on the data'''
n_neighbors = 2
weights = 'uniform'
clf = sklearn.neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
clf.fit(X_train, y_train)

'''Make predictions on the test data using the trained classifier'''
y_predicted = clf.predict(X_test)
print ('Classification report:')
print sklearn.metrics.classification_report(y_test, y_predicted,
                                            target_names=data_test.target_names)


'''HW0: Warm Up by UNI:jg3555'''

number_of_neighbors = np.arange(2,51)
f1_scores = []
for n_neighbors_plt in number_of_neighbors:

    clf_plt = sklearn.neighbors.KNeighborsClassifier(n_neighbors_plt, weights=weights)
    clf_plt.fit(X_train, y_train)
    y_predicted = clf_plt.predict(X_test)
    f1_score = sklearn.metrics.f1_score(y_test, y_predicted, average = 'weighted')
    f1_scores.append(f1_score)

f1_scores = np.asarray(f1_scores)
plt.plot(number_of_neighbors, f1_scores)
plt.xlabel('Number of Neighbors')
plt.ylabel('F1 Scores')
plt.show()

