# config {stack,buffer,label}
def get_features(config,sent_dict):
    features = []

    if len(config[0]) > 0:
        # Top of stack.
#        top = config[0][-1]
#        # Token
#        top_stk_token_feature = 'TOP_STK_TOKEN_'+str(sent_dict['FORM'][top].lower())
#        features.append(top_stk_token_feature)
        
        stk_length = 'STK_LENGTH_'+str(len(config[0]))
        features.append(stk_length)
        
        if len(config[1]) > 0:
            label  = ['STK', 'BUF']
            for i in range(2):
                top = config[i][-1]
                top_token_cpostag = 'TOP_'+label[i]+'_CPOSTAG_'+str(sent_dict['CPOSTAG'][top].lower())
                top_token_lemma_cpostag = 'TOP_'+label[i]+'_LEMMA_'+\
                                 str(sent_dict['LEMMA'][top].lower())+'_CPOSTAG_'+\
                                 str(sent_dict['CPOSTAG'][top].lower())
                
                features.append(top_token_lemma_cpostag)
                features.append(top_token_cpostag)
#
            top_tokens_cpostag = 'TOP_CPOSTAGS_STK_'+\
                                 str(sent_dict['CPOSTAG'][config[0][-1]].lower())+'_BUF_'+\
                                 str(sent_dict['CPOSTAG'][config[1][-1]].lower())
            features.append(top_tokens_cpostag)
        for i in range(2):
            label = ['STK', 'BUF']
            if len(config[i]) >= 2:
                top = config[i][-1]
                second = config[i][-2]
        
                two_stk_tokens_cpostag = 'TWO_CPOSTAGS_'+label[i]+\
                                     str(sent_dict['CPOSTAG'][second].lower())+'_'+\
                                     str(sent_dict['CPOSTAG'][top].lower())
                features.append(two_stk_tokens_cpostag)
  
        if len(config[0]) >= 2:
            top = config[0][-1]
            second = config[0][-2]
           
            for position in (top, second):
                    if str(sent_dict['LEMMA'][position]) == '_':
                        word = str(sent_dict['FORM'][position].lower())
                    else:
                        word = str(sent_dict['LEMMA'][position].lower())
                    
                    two_stk_tokens_lemma_cpostag = 'STK_LEMMA_'+ word+'_TWO_CPOSTAGS_'+\
                    str(sent_dict['CPOSTAG'][second].lower())+'_'+\
                    str(sent_dict['CPOSTAG'][top].lower())   
                    
                    features.append(two_stk_tokens_lemma_cpostag)
            if len(config[0]) > 3:
                three_stk_tokens_cpostag = 'THREE_STK_CPOSTAGS_'+\
                                     str(sent_dict['CPOSTAG'][config[0][-3]].lower())+'_'+\
                                     str(sent_dict['CPOSTAG'][second].lower())+'_'+\
                                     str(sent_dict['CPOSTAG'][top].lower())
                features.append(three_stk_tokens_cpostag)
#                        
        
    return features
