def shift(stack, buff,  dgraph):
    stack.append(buff.pop(-1))

    
def left_arc(stack, buff, dgraph):
    #last item in stack is now the HEAD, second to last is the dependent
    #dgraph append (dependent, HEAD) 
    dgraph.append((stack.pop(-2), stack[-1]))
    
    
def right_arc(stack, buff, dgraph):
    #Second to last item in stack is now the HEAD, and last is the dependent
    #dgraph append (dependent, HEAD) 
    dgraph.append((stack.pop(-1), stack[-1]))


def oracle_std(stack, buff, dgraph, gold_arcs):
    if len(stack) <= 1:
        return "shift"
    else:
        next_arcs = [x for x in gold_arcs if x not in dgraph]
        heads = [next_arc[1] for next_arc in next_arcs]
#        excl = list(set(stack)-set(heads))
        left_arc = (stack[-2], stack[-1])
        right_arc = (stack[-1], stack[-2])
        
        if left_arc in next_arcs:
            return "left_arc"
        elif right_arc in next_arcs:
            if right_arc[0] not in heads:
                return "right_arc"
            else:
                return "shift"
        else:
            return "shift"

#        result = "shift"
#        for next_arc in next_arcs:
#            dependent = next_arc[0]
#            #This condition makes sure all children of a word are collected before parsing into an arc
#            if dependent in excl:
#                if left_arc in next_arcs:
#                    return "left_arc"
#                elif next_arc == right_arc:
#                    return "right_arc"
#                
#    return result

def make_transitions(buff, oracle, gold_arcs=None):
    stack = []
    dgraph = []
    configurations = []
    while (len(buff) > 0 or len(stack) > 1):
        choice = oracle(stack, buff, dgraph, gold_arcs)
        # Makes a copy. Else configuration has a reference to buff and stack.
        config_buff = list(buff)
        config_stack = list(stack)
        configurations.append([config_stack,config_buff,choice])
#        print(choice, stack, buff, dgraph)
        if choice == 'shift':	shift(stack, buff, dgraph)
        elif choice == 'left_arc': left_arc(stack, buff, dgraph)
        elif choice == 'right_arc': right_arc(stack, buff, dgraph)
        else: return None
    return dgraph,configurations
