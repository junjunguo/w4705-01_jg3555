HW2 Parsing
W4705_001 - Natural Language Processing
Prof. Kathleen McKeown
Submission date : October 14th, 2017

Name:Jun Guo
Uni:jg3555
Email: jg3555@columbia.edu

This assignment will comprise of tasks in unlabeled dependency parsing. Submit the answers to the below questions along
with checking in your code.

Part 1 - Arc Standard
1. Output of your validate_transitions.py

FORMS : ['ROOT', 'It', 's', 'now', 'my', 'home', '.']
GOLD ARCS : [(1, 5), (2, 5), (3, 5), (4, 5), (5, 0), (6, 5)]
TRANSITIONS : 
shift
shift
shift
shift
shift
shift
left_arc
left_arc
left_arc
left_arc
shift
right_arc
right_arc

2. List all the feature types you implemented. [Ex: ‘TOP_STK_TOKEN_’ , ‘...’, ‘....’]
For our reference.

a. stk_token_length: length of the stack (’STK_LENGTH’+length)
b. top_token_cpostag: coarse postag for the end words from both stack and buffer. (’TOP_’+ (buffer or stack)+’_CPOSTAG_’ + CPOSTAG)
c. top_token_lemma_cpostag: for the end words from both stack and buffer, the lemma and the coarse postag. (’TOP_’+ (‘BUF’ or ‘STK’)+’_LEMMA_’ + LEMMA+ ’_CPOSTAG_’ + CPOSTAG)
d. top_tokens_cpostag: Two top tokens’ cpostag from stack and buffer respectively (‘TOP_CPOSTAGS_STK_’+CPOSTAG[top of buffer]+’_BUF_’+CPOSTAG[top of stack])
e. two_stk_tokens_cpostag: Top and second to top tokens from either stack or buffer.
(’TWO_CPOSTAGS_’+(‘BUF’ or ‘STK’)+ CPOSTAG(second)+CPOSTAG(top)
f. two_stk_token_lemma_cpostag: One lemma and two coarse postag.
(‘STK_LEMMA_’+word(lemma unless not available then form)+’_TWO_CPOSTAG_’+CPOSTAG(second)+CPOSTAG(top))
g. three_stk_tokens_cpostag 
(’THREE_STK_CPOSTAGS’+CPOSTAG(third)+CPOSTAG(second)+CPOSTAG(top))


3. Explain any 6 feature types in detail. What does the feature type represent? Why do you think this is useful for transition predictions?

b. top_token_cpostag takes the coarse pos tag of top token from either stack or buffer. The feature is useful because it is the denominator of conditional probability of certain lemma given a postag. 

c. top_token_lemma_cpostag takes the lemma and cpostag of top token from either stack or buffer making it a feature. I think the feature is significant because I believe the postag and the corresponding lemma this is the numerator of conditional probability of lemma given the postag.

d. top_tokens_cpostag: Using two postags from stack and buffer respectively. Using information in the buffer I think reflect the actual structure of the sentence and relationships between words. 
 
e. two_stk_tokens_cpostag: I think the fact that forming an arc or shift doesn’t simply depend on one word or its pos tag. Having two top pos tags in the stack would help predict the next choice more accurately. 

f. two_stk_token_lemma_cpostag: The feature type takes two top cpostag and one lemma of the two tokens. I think the structure reflect chain of words and how pair of word interact with each other. 

g. three_stk_tokens_cpostag: Using three consecutive coarse pos tags, I believe would be beneficial for more complicated sentence. 



4. How do you interpret precision and recall in context of this task?

Precision is number of correct prediction out of all the prediction. 
Recall is the number of correct prediction out of total transitions (gold_arcs).
  
5. What is your final F1 score on the dev set? 

F1 score is 0.930638256822101

Part 2 - Domain Adaptation
6. Average F1 score from 10 runs of domain_adaptation_eval.py.
As it is a smaller training set you may notice varying F1 scores. Write the average score over 10 runs. The scores you
report are a reference. We will be running your code separately for grading.
Format for each line-
‘train_genre :  <train_genre>, test_genre : <test_genre>, Avg F1 : <F1_score>’
‘train_genre :  <reviews>, test_genre : <email>, Avg F1 : <0.8252934313>’
‘train_genre :  <reviews>, test_genre : <newsgroup>, Avg F1 : <0.8622579467>’

7. Provide an explanation of the performance of the feature types in domain adaptation. What features generalized well? What features did not?
Stack Length, Top Token Cpostag, Top Token Lemma and Token, Two Tokens between buffer and stack, Two Tokens among buffer, and Two Tokens from stack. These features generalized well. 

After looking into the 15 best performing features, I found out that top token in buffer played an important role in predicting ‘shift’ and ‘left_arc’, especially when it is an punctuation. I think this make sense since punctuation marks usually separate parts in sentences. The word in front of the stack is unlikely to form a right_arc. When the top token is a noun, ‘shift’ is also likely to be predicted. When the second and top terms in a stack has cpostags adj. and noun, the prediction is more likely to be left arc. When the stack length is 2, the stack is very likely to perform “right_arc”. I think this is because at the end of the parsing process, the root would form a right arc to the last token in stack. 

Above are some of my understandings after reading features and their coefficient. I have taken out some features that worked in the original evaluation but not the domain adapted model. These features include the one that combine one lemma and two tokens in a stack, and the features with coarse pos tag of three tokens. After trying out these feature, I found out that these will lower the evaluation scores. I think I might have been overfitting the model by adding to many features. 


PROVIDED CODE -

dataset.py - Module with the load_data function given a web treebank data set filename.
dependency.py - Module with all the functions pertaining to the arc standard algorithm. shift, right_arc, left_arc, oracle_std, make_transition.
domain_adaptation.py - Module with the train function for domain adaptation.
domain_adaptation.pu - Module to test and evaluate domain adaptation.
en-ud-dev.conllu - WebTreebank dev dataset.
en-ud-train.conllu - WebTreebank train dataset.
feature_extraction.py - Module that defines the feature_extractor function.
feature_extraction_da.py - Module that defines the feature_extractor for the domain adaptation experiment.
train.py - Module for training a LinearSVC model for transition predictions.
utils.py - Module with helper functions.
validate_transitions.py - Module to validate the function in dependency.py.
